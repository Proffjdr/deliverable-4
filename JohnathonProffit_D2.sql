/*
USE master
DROP database JohnathonProffitt_D2
CREATE database JohnathonProffitt_D2
*/
USE JohnathonProffitt_D2

GO

CREATE TYPE password
FROM varchar(30) NOT NULL;

CREATE TYPE username
FROM varchar(12) NOT NULL;

CREATE TYPE person_Name
FROM varchar(25) NOT NULL;

CREATE TYPE email
FROM varchar(50) NOT NULL;

CREATE TYPE phone
FROM varchar(7) NOT NULL;

CREATE TYPE areacode
FROM varchar(3) NOT NULL;

GO

create table Car(
	licence varchar(6) not null,
	make varchar(50) not null,
	primary key (licence)
)

create table Instructor(
	username username,
	password password,
	fname person_Name,
	sname person_Name,
	email email,
	areacode areacode,
	phone phone,
	primary key (username),
	CHECK(email like '%_@_%._%'),
	CHECK(areacode like '[0-9][0-9]' OR areacode like '[0-9][0-9][0-9]'),
	CHECK(phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	CHECK(LEN(password) > 5)
)

create table Type (
	name varchar(11) NOT NULL,
	cost int NOT NULL,
	hours int NOT NULL,
	primary key (name),
	CHECK(name like 'new' OR name like 'experienced')
	)

create table Client (
	username username,
	password password,
	fname person_name,
	sname person_name,
	email email,
	areacode areacode,
	phone phone,
	type varchar(11),
	primary key (username),
	foreign key(type) references Type(name),
	CHECK(email like '%_@_%._%'),
	CHECK(areacode like '[0-9][0-9]' OR areacode like '[0-9][0-9][0-9]'),
	CHECK(phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	CHECK(LEN(password) > 5)
	)

create table Timeslot(
	timeslot_day date,
	timeslot_time time(0),
	primary key (timeslot_day, timeslot_time),
	)
	
create Table Available(
	Instructor_username username,
	timeslot_day date,
	timeslot_time time(0),
	Available bit DEFAULT 0,
	Booked bit DEFAULT 0,
	foreign key (Instructor_username) references Instructor(username),
	foreign key (timeslot_day, timeslot_time) references Timeslot(timeslot_day, timeslot_time),
	primary key (Instructor_username, timeslot_day, timeslot_time)
)


create table Appointment(
	id int IDENTITY(1,1),
	notes varchar(255),
	Client_username username,
	Instructor_username username,
	Timeslot_time time(0) NOT NULL,
	Timeslot_day date NOT NULL,
	Car varchar(6),
	Confirmed bit DEFAULT 0,
	Complete bit DEFAULT 0,
	Billed bit DEFAULT 0,
	Paid bit DEFAULT 0,
	primary key (id),
	foreign key (Instructor_username, Timeslot_day, Timeslot_time) references Available,
	foreign key (Client_username) references Client(username),
	foreign key (Car) references Car(licence)
)


create table Document(
	id int IDENTITY(1,1),
	date date NOT NULL,
	link varchar(50) NOT NULL,
	type varchar(11) NOT NULL,
	Client_username username,
	CHECK(type like 'bill' OR type like 'certificate'),
	primary key (id),
	foreign key (type) references Type(name),
	foreign key (Client_username) references Client(username)
	)

create table Admin (
	username username,
	password password,
	fname person_name,
	sname person_name,
	email email,
	primary key (username),
	CHECK(email like '%_@_%._%'),
	CHECK(LEN(password) > 5)
	)

create table Assigned(
	username username,	
	licence varchar(6),
	timeslot_day date,
	timeslot_time time(0),
	primary key (licence, username, timeslot_day, timeslot_time),
	foreign key (licence) references Car(licence),
	foreign key (username) references Instructor(username),
	foreign key (timeslot_day, timeslot_time) references Timeslot
	)

GO
	
INSERT into Car values ('FAP322','Toyota Starlet');
INSERT into Car values ('RAY007','Aston-Martin Vanquish');
INSERT into Car values ('VROOMM','Nissan Maxima');
INSERT into Car values ('ABC123','Kia Rio');
INSERT into Car values ('CAR666','Mitsubishi Diamante');

INSERT into Type values('new', 250, 5);
INSERT into Type values('experienced', 100, 2);

INSERT into Instructor values('Stig','drivefast','The','Stig','TheStig@topgear.co.uk','021','2334565');
INSERT into Instructor values('TDriver','driver','Tom','Driver','TDriver@gmail.com','07','5551345');
INSERT into Instructor values('MaxPower','Powerful','Max','Powers','MaxPowers@tothemax.org','022','0987654');
INSERT into Instructor values('Jack','password','Jack','Knife','JackKnife@gmail.com','07','5551234');

INSERT into Admin values('Doris','pass123','Doris','Dotty','Doris@yahoo.com');
INSERT into Admin values('Beat','Icanneverrememberthese','Beatrice','Knowles','Beatrice@orcon.net.nz');
INSERT into Admin values('Adam','thequickbrownfox','Adam','French','Adam@gmail.org');

GO

DECLARE @counttime INT;
DECLARE @countday INT;
DECLARE @day date;
DECLARE @time time;

SET @countday = 0;
SET @day = CONVERT(date,'01/05/2017', 103); -- first day to be inserted dd/mm/yyyy
set @time= CONVERT(time, '07:00'); -- first timeslot to be inserted hh:mm

WHILE(@countday < 90) -- number of days of hourly shifts to be inserted
BEGIN

IF(DATENAME(WEEKDAY,@day) <> 'Saturday' AND DATENAME(WEEKDAY,@day)<>'Sunday') 
BEGIN
		SET @counttime = 1
		WHILE(@counttime <= 13) -- number of hourly shifts to be inserted
			BEGIN 
				INSERT into Timeslot Values(@day,DATEADD(hh,@counttime-1,@time))
				SET @counttime = @counttime+1;
			END;
		SET @day = DATEADD(dd,1,@day);
		SET @countday=@countday+1;
END;

Else IF(DATENAME(WEEKDAY, @day) like 'Saturday')
	BEGIN
		SET @counttime = 1
		WHILE(@counttime <= 13) -- number of hourly shifts to be inserted
			BEGIN 
				INSERT into Timeslot Values(@day,DATEADD(hh,@counttime-1,@time))
				SET @counttime = @counttime+1;
			END;
			SET @day = DATEADD(dd,1,@day);
			SET @countday=@countday+1;
			
			
	END;
ELSE IF(DATENAME(WEEKDAY,@day) like 'Sunday')
BEGIN
SET @day = DATEADD(dd,1,@day);
SET @countday=@countday+1;

END;
END;

GO

Insert into Available(Instructor_username, timeslot_day, timeslot_time) SELECT username, timeslot_day, timeslot_time FROM Instructor, Timeslot 

GO

UPDATE Available SET Available = '1' WHERE timeslot_time <'17:00:00' and timeslot_time > '11:00:00'


SELECT * FROM Instructor;
SELECT * FROM Client;
SELECT * FROM Admin;
SELECT * FROM Timeslot;
SELECT * FROM Available;
SELECT * FROM Appointment;
SELECT * FROM Document;
SELECT * FROM Type;
SELECT * FROM Car;
SELECT * FROM Assigned;

