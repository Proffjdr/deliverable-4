﻿namespace Deliverable_2
{
    partial class Stats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.buttonBack = new System.Windows.Forms.Button();
            this.chartBookings = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartAvail = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.ButtonUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chartBookings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartAvail)).BeginInit();
            this.SuspendLayout();
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(13, 13);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 0;
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(13, 335);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(75, 23);
            this.buttonBack.TabIndex = 3;
            this.buttonBack.Text = "Back";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // chartBookings
            // 
            chartArea1.AxisX.Title = "Instructors";
            chartArea1.AxisY.Title = "Bookings";
            chartArea1.Name = "ChartArea1";
            this.chartBookings.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartBookings.Legends.Add(legend1);
            this.chartBookings.Location = new System.Drawing.Point(253, 13);
            this.chartBookings.Name = "chartBookings";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Bookings";
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Single;
            this.chartBookings.Series.Add(series1);
            this.chartBookings.Size = new System.Drawing.Size(300, 300);
            this.chartBookings.TabIndex = 4;
            this.chartBookings.Text = "chart1";
            // 
            // chartAvail
            // 
            chartArea2.AxisX.Title = "Instructors";
            chartArea2.AxisY.Title = "Available Hours";
            chartArea2.Name = "ChartArea1";
            this.chartAvail.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartAvail.Legends.Add(legend2);
            this.chartAvail.Location = new System.Drawing.Point(584, 13);
            this.chartAvail.Name = "chartAvail";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Availability";
            series2.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Single;
            this.chartAvail.Series.Add(series2);
            this.chartAvail.Size = new System.Drawing.Size(300, 300);
            this.chartAvail.TabIndex = 5;
            this.chartAvail.Text = "chart1";
            // 
            // ButtonUpdate
            // 
            this.ButtonUpdate.Location = new System.Drawing.Point(132, 188);
            this.ButtonUpdate.Name = "ButtonUpdate";
            this.ButtonUpdate.Size = new System.Drawing.Size(107, 42);
            this.ButtonUpdate.TabIndex = 6;
            this.ButtonUpdate.Text = "Update";
            this.ButtonUpdate.UseVisualStyleBackColor = true;
            this.ButtonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // Stats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 370);
            this.Controls.Add(this.ButtonUpdate);
            this.Controls.Add(this.chartAvail);
            this.Controls.Add(this.chartBookings);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.monthCalendar1);
            this.Name = "Stats";
            this.Text = "Stats";
            this.Load += new System.EventHandler(this.Stats_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartBookings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartAvail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartBookings;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAvail;
        private System.Windows.Forms.Button ButtonUpdate;
    }
}