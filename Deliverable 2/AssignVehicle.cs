﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deliverable_2
{
    public partial class AssignVehicle : Form
    {
        public AssignVehicle()
        {
            InitializeComponent();
            update();
        }

        private void update()
        {
            comboBoxInst.Text = "";
            comboBoxInst.Items.Clear();
            comboBoxDateFrom.Text = "";
            comboBoxDateFrom.Items.Clear();
            comboBoxDateTo.Text = "";
            comboBoxDateTo.Items.Clear();
            comboBoxVehicle.Text = "";
            comboBoxVehicle.Items.Clear();
            
            SQL.editComboBoxItems(comboBoxInst, "Select username FROM Instructor");
            SQL.selectQuery($"Select DISTINCT Timeslot_day from Timeslot");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    var Date = DateTime.Parse($"{SQL.read[0].ToString()}");
                    var DateString = Date.ToString("yyyy-MM-dd");
                    comboBoxDateFrom.Items.Add(DateString);
                }
            }

        }

        private void comboBoxDateFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxDateTo.Text = "";
            comboBoxDateTo.Items.Clear();
            comboBoxVehicle.Text = "";
            comboBoxVehicle.Items.Clear();
            SQL.selectQuery($"Select DISTINCT Timeslot_day from Timeslot WHERE Timeslot_day > '{comboBoxDateFrom.Text}'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    var Date = DateTime.Parse($"{SQL.read[0].ToString()}");
                    var DateString = Date.ToString("yyyy-MM-dd");
                    comboBoxDateTo.Items.Add(DateString);
                }
            }

        }

        private void comboBoxDateTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            SQL.editComboBoxItems(comboBoxVehicle, $"Select licence from Car where licence NOT IN (SELECT licence from Assigned Where timeslot_day BETWEEN '{comboBoxDateFrom.Text}' AND '{comboBoxDateTo.Text}')");
        }

        private void buttonAssign_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show($"Are you sure to proceed? This will overwrite existing Assignments!",
                                     "Confirm Assignment?",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                SQL.executeQuery($"DELETE FROM Assigned WHERE username like '{comboBoxInst}' AND timeslot_day BETWEEN '{comboBoxDateFrom.Text}' AND '{comboBoxDateTo.Text}'");
                SQL.executeQuery($"INSERT INTO Assigned (username, licence, timeslot_day, timeslot_time) SELECT username, licence, timeslot_day, timeslot_time FROM Instructor, Car, Timeslot WHERE username like '{comboBoxInst.Text}' AND licence like '{comboBoxVehicle.Text}' and timeslot_day BETWEEN '{comboBoxDateFrom.Text}' AND '{comboBoxDateTo.Text}'");
                MessageBox.Show("Assignment updated");
                update();
            }
            else
            {
                update();
            }
            
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminHome register = new AdminHome();
            register.ShowDialog();
            this.Close();
        }
    }
}
