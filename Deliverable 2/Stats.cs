﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deliverable_2
{
    public partial class Stats : Form
    {
        public Stats()
        {
            InitializeComponent();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            Coverage register = new Coverage();
            register.ShowDialog();
            this.Close();

        }

        private void Stats_Load(object sender, EventArgs e)
        {
            
        }

        private void update()
        {
            foreach (var series in chartBookings.Series)
            {
                series.Points.Clear();
            }

            foreach (var series in chartAvail.Series)
            {
                series.Points.Clear();
            }

            string StartDate;
            string EndDate;
            StartDate = monthCalendar1.SelectionRange.Start.ToString("yyyy-MM-dd");
            EndDate = monthCalendar1.SelectionRange.End.ToString("yyyy-MM-dd");

            List<string> Inst = new List<string>();
            Inst.Clear();

            SQL.selectQuery("SELECT DISTINCT Instructor_username FROM Available");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Inst.Add(SQL.read[0].ToString());
                }
            }

            foreach (string name in Inst)
            {
                SQL.selectQuery($"SELECT COUNT(Instructor_username) FROM Available WHERE Instructor_Username like '{name}' AND Booked like '1' AND timeslot_day >= '{StartDate}' AND timeslot_day <= '{EndDate}'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        var count = SQL.read[0].ToString();
                        chartBookings.Series["Bookings"].Points.AddXY(name, int.Parse(count));
                    }
                }

            }

            foreach (string name in Inst)
            {
                SQL.selectQuery($"SELECT COUNT(Instructor_username) FROM Available WHERE Instructor_Username like '{name}' AND Available like '1' AND timeslot_day >= '{StartDate}' AND timeslot_day <= '{EndDate}'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        var count = SQL.read[0].ToString();
                        chartAvail.Series["Availability"].Points.AddXY(name, int.Parse(count));
                    }
                }

            }

        }


        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            update();
        }
    }
}
