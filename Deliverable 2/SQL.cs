﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;


namespace Deliverable_2
{
    class SQL
    {
        public static SqlConnection con = new SqlConnection(@"Data Source=localhost\;Database=JohnathonProffitt_D2;Integrated Security=True");
        public static SqlCommand cmd = new SqlCommand();
        public static SqlDataReader read;
        public static SqlDataAdapter dataAdapter;
        public static SqlCommandBuilder commandBuilder;
        public static DataTable table1 = new DataTable();
        public static BindingSource Source1 = new BindingSource();


        public static void executeQuery(string query)
        {
            try
            {
                con.Close();
                cmd.Connection = con;
                con.Open();
                cmd.CommandText = query;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        public static void selectQuery(string query)
        {
            try
            {
                con.Close();
                cmd.Connection = con;
                con.Open();
                cmd.CommandText = query;
                read = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        /// <summary>
        /// Prints out the ID  based on the query givin into a combo box
        /// </summary>
        /// <param name="comboBox">A control to be used to write existing names to</param>
        /// <param name="query">An SQL query to generate from</param>
        public static void editComboBoxItems(ComboBox comboBox, string query)
        {
            bool clear = true;

            //gets data from database
            SQL.selectQuery(query);
            //Check that there is something to write brah
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (comboBox.Text == SQL.read[0].ToString())
                    {
                        clear = false;
                    }
                }
            }

            //gets data from database
            SQL.selectQuery(query);
            //if nothing in the comboBox then we need to clear it
            if (clear)
            {
                comboBox.Text = "";
                comboBox.Items.Clear();

            }

            // this will print whatever is in the database to the combobox
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    comboBox.Items.Add(SQL.read[0].ToString());
                }
            }
        }

        public static void editComboBoxItems2(ComboBox comboBox, string query)
        {
            bool clear = true;
            string combotext = "blarf";

            //gets data from database
            SQL.selectQuery(query);
            //Check that there is something to write brah
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (comboBox.Text == combotext)
                    {
                        clear = false;
                    }
                }
            }

            //gets data from database
            SQL.selectQuery(query);
            //if nothing in the comboBox then we need to clear it
            if (clear)
            {
                comboBox.Text = "";
                comboBox.Items.Clear();

            }

            // this will print whatever is in the database to the combobox
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    combotext = $"{SQL.read[0].ToString()} {SQL.read[1].ToString()}";
                    comboBox.Items.Add(combotext);
                }
            }
        }

        public static void FillGridView(DataGridView gridview, string query)
        {
            table1.Rows.Clear();
            table1.Columns.Clear();
            con.Close();
            dataAdapter = new SqlDataAdapter(query, con);
            commandBuilder = new SqlCommandBuilder(dataAdapter);
            table1.Locale = System.Globalization.CultureInfo.InvariantCulture;
            dataAdapter.Fill(table1);
            Source1.DataSource = table1;
            gridview.DataSource = Source1;
        }


        public static void SaveGrid()
        {
            dataAdapter.Update(table1);
            MessageBox.Show("Information Updated!");
        }

        public static void FillList(ListBox listview, string query)
        {
            listview.Items.Clear();
            SQL.selectQuery(query);
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    listview.Items.Add(SQL.read[0].ToString());
                }
            }

        }
    }
}
    


