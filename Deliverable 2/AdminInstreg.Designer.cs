﻿namespace Deliverable_2
{
    partial class AdminInstreg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonReturn = new System.Windows.Forms.Button();
            this.monthCalendarInst = new System.Windows.Forms.MonthCalendar();
            this.gridAvailable = new System.Windows.Forms.DataGridView();
            this.buttonSaveAvailability = new System.Windows.Forms.Button();
            this.comboBoxInst = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridAvailable)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonReturn
            // 
            this.buttonReturn.Location = new System.Drawing.Point(598, 346);
            this.buttonReturn.Name = "buttonReturn";
            this.buttonReturn.Size = new System.Drawing.Size(75, 23);
            this.buttonReturn.TabIndex = 10;
            this.buttonReturn.Text = "Return";
            this.buttonReturn.UseVisualStyleBackColor = true;
            this.buttonReturn.Click += new System.EventHandler(this.buttonReturn_Click);
            // 
            // monthCalendarInst
            // 
            this.monthCalendarInst.Location = new System.Drawing.Point(12, 12);
            this.monthCalendarInst.Name = "monthCalendarInst";
            this.monthCalendarInst.TabIndex = 9;
            this.monthCalendarInst.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarInst_DateChanged);
            // 
            // gridAvailable
            // 
            this.gridAvailable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridAvailable.Location = new System.Drawing.Point(251, 12);
            this.gridAvailable.Name = "gridAvailable";
            this.gridAvailable.Size = new System.Drawing.Size(422, 328);
            this.gridAvailable.TabIndex = 8;
            this.gridAvailable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridAvailable_CellContentClick);
            // 
            // buttonSaveAvailability
            // 
            this.buttonSaveAvailability.Location = new System.Drawing.Point(12, 295);
            this.buttonSaveAvailability.Name = "buttonSaveAvailability";
            this.buttonSaveAvailability.Size = new System.Drawing.Size(120, 45);
            this.buttonSaveAvailability.TabIndex = 7;
            this.buttonSaveAvailability.Text = "Save Availability";
            this.buttonSaveAvailability.UseVisualStyleBackColor = true;
            this.buttonSaveAvailability.Click += new System.EventHandler(this.buttonSaveAvailability_Click);
            // 
            // comboBoxInst
            // 
            this.comboBoxInst.FormattingEnabled = true;
            this.comboBoxInst.Location = new System.Drawing.Point(118, 186);
            this.comboBoxInst.Name = "comboBoxInst";
            this.comboBoxInst.Size = new System.Drawing.Size(121, 21);
            this.comboBoxInst.TabIndex = 11;
            this.comboBoxInst.SelectedIndexChanged += new System.EventHandler(this.comboBoxInst_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 186);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Instructor";
            // 
            // AdminInstreg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 387);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxInst);
            this.Controls.Add(this.buttonReturn);
            this.Controls.Add(this.monthCalendarInst);
            this.Controls.Add(this.gridAvailable);
            this.Controls.Add(this.buttonSaveAvailability);
            this.Name = "AdminInstreg";
            this.Text = "Instructor Availability Management";
            ((System.ComponentModel.ISupportInitialize)(this.gridAvailable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonReturn;
        private System.Windows.Forms.MonthCalendar monthCalendarInst;
        private System.Windows.Forms.DataGridView gridAvailable;
        private System.Windows.Forms.Button buttonSaveAvailability;
        private System.Windows.Forms.ComboBox comboBoxInst;
        private System.Windows.Forms.Label label1;
    }
}