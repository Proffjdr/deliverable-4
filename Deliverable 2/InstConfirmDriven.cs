﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deliverable_2
{
    public partial class InstConfirmDriven : Form
    {
        public InstConfirmDriven()
        {
            InitializeComponent();
            
        }

        private void update()
        {
            string StartDate;
            string EndDate;
            StartDate = monthCalendarInst.SelectionRange.Start.ToString("yyyy-MM-dd");
            EndDate = monthCalendarInst.SelectionRange.End.ToString("yyyy-MM-dd");


            SQL.FillGridView(gridComplete, $"SELECT ID, Client_username, Timeslot_day, Timeslot_time, Confirmed, Complete, notes from Appointment where Instructor_username like '{Login.User}' AND timeslot_day >= '{StartDate}' AND timeslot_day <= '{EndDate}' AND Confirmed like '1' AND Billed like '0'");

            gridComplete.AllowUserToResizeRows = false;
            gridComplete.AllowUserToResizeColumns = false;
            gridComplete.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridComplete.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridComplete.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridComplete.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridComplete.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridComplete.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            gridComplete.RowHeadersVisible = false;
            gridComplete.Columns[1].ReadOnly = true;
            gridComplete.Columns[2].ReadOnly = true;
            gridComplete.Columns[3].ReadOnly = true;
            gridComplete.Columns[4].ReadOnly = true;
            gridComplete.AllowUserToAddRows = false;
            gridComplete.Columns[1].HeaderText = "Client Name";
            gridComplete.Columns[2].HeaderText = "Date";
            gridComplete.Columns[3].HeaderText = "Time";
            gridComplete.Columns[4].HeaderText = "Confirmed";
            gridComplete.Columns[5].HeaderText = "Completed";
            gridComplete.Columns[6].HeaderText = "Notes";
            gridComplete.Columns[0].Visible = false;


        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            SQL.SaveGrid();
            update();
        }


        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            InstructorHome register = new InstructorHome();
            register.ShowDialog();
            this.Close();
        }



        private void monthCalendarInst_DateChanged(object sender, DateRangeEventArgs e)
        {
            update();
        }

        private void InstConfirmDriven_Load(object sender, EventArgs e)
        {
            update();
        }
    }
}
