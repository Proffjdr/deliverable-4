﻿namespace Deliverable_2
{
    partial class RegisterInstructor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxInstConfirmPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxInstPhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxInstAreaCode = new System.Windows.Forms.TextBox();
            this.textBoxInstEmail = new System.Windows.Forms.TextBox();
            this.textBoxInstSName = new System.Windows.Forms.TextBox();
            this.textBoxInstFName = new System.Windows.Forms.TextBox();
            this.textBoxInstPassword = new System.Windows.Forms.TextBox();
            this.textBoxInstUserName = new System.Windows.Forms.TextBox();
            this.buttonInstRegister = new System.Windows.Forms.Button();
            this.buttonAdminReturn = new System.Windows.Forms.Button();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(45, 119);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 13);
            this.label9.TabIndex = 34;
            this.label9.Text = "Confirm Password";
            // 
            // textBoxInstConfirmPassword
            // 
            this.textBoxInstConfirmPassword.Location = new System.Drawing.Point(48, 135);
            this.textBoxInstConfirmPassword.Name = "textBoxInstConfirmPassword";
            this.textBoxInstConfirmPassword.Size = new System.Drawing.Size(337, 20);
            this.textBoxInstConfirmPassword.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 314);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Phone: Number";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 275);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "Phone: Areacode";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 236);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "EMail";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "Last Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "First Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Username";
            // 
            // textBoxInstPhoneNumber
            // 
            this.textBoxInstPhoneNumber.Location = new System.Drawing.Point(48, 330);
            this.textBoxInstPhoneNumber.Name = "textBoxInstPhoneNumber";
            this.textBoxInstPhoneNumber.Size = new System.Drawing.Size(337, 20);
            this.textBoxInstPhoneNumber.TabIndex = 25;
            // 
            // textBoxInstAreaCode
            // 
            this.textBoxInstAreaCode.Location = new System.Drawing.Point(48, 291);
            this.textBoxInstAreaCode.Name = "textBoxInstAreaCode";
            this.textBoxInstAreaCode.Size = new System.Drawing.Size(87, 20);
            this.textBoxInstAreaCode.TabIndex = 24;
            // 
            // textBoxInstEmail
            // 
            this.textBoxInstEmail.Location = new System.Drawing.Point(48, 252);
            this.textBoxInstEmail.Name = "textBoxInstEmail";
            this.textBoxInstEmail.Size = new System.Drawing.Size(337, 20);
            this.textBoxInstEmail.TabIndex = 23;
            // 
            // textBoxInstSName
            // 
            this.textBoxInstSName.Location = new System.Drawing.Point(50, 213);
            this.textBoxInstSName.Name = "textBoxInstSName";
            this.textBoxInstSName.Size = new System.Drawing.Size(335, 20);
            this.textBoxInstSName.TabIndex = 22;
            // 
            // textBoxInstFName
            // 
            this.textBoxInstFName.Location = new System.Drawing.Point(48, 174);
            this.textBoxInstFName.Name = "textBoxInstFName";
            this.textBoxInstFName.Size = new System.Drawing.Size(337, 20);
            this.textBoxInstFName.TabIndex = 21;
            // 
            // textBoxInstPassword
            // 
            this.textBoxInstPassword.Location = new System.Drawing.Point(48, 96);
            this.textBoxInstPassword.Name = "textBoxInstPassword";
            this.textBoxInstPassword.Size = new System.Drawing.Size(337, 20);
            this.textBoxInstPassword.TabIndex = 19;
            // 
            // textBoxInstUserName
            // 
            this.textBoxInstUserName.Location = new System.Drawing.Point(48, 57);
            this.textBoxInstUserName.Name = "textBoxInstUserName";
            this.textBoxInstUserName.Size = new System.Drawing.Size(337, 20);
            this.textBoxInstUserName.TabIndex = 18;
            // 
            // buttonInstRegister
            // 
            this.buttonInstRegister.Location = new System.Drawing.Point(48, 398);
            this.buttonInstRegister.Name = "buttonInstRegister";
            this.buttonInstRegister.Size = new System.Drawing.Size(337, 58);
            this.buttonInstRegister.TabIndex = 35;
            this.buttonInstRegister.Text = "REGISTER";
            this.buttonInstRegister.UseVisualStyleBackColor = true;
            this.buttonInstRegister.Click += new System.EventHandler(this.buttonInstRegister_Click);
            // 
            // buttonAdminReturn
            // 
            this.buttonAdminReturn.Location = new System.Drawing.Point(178, 473);
            this.buttonAdminReturn.Name = "buttonAdminReturn";
            this.buttonAdminReturn.Size = new System.Drawing.Size(75, 23);
            this.buttonAdminReturn.TabIndex = 36;
            this.buttonAdminReturn.Text = "Return";
            this.buttonAdminReturn.UseVisualStyleBackColor = true;
            this.buttonAdminReturn.Click += new System.EventHandler(this.buttonAdminReturn_Click);
            // 
            // comboBoxType
            // 
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Location = new System.Drawing.Point(48, 371);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxType.TabIndex = 37;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 352);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 38;
            this.label8.Text = "Staff Type";
            // 
            // RegisterInstructor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 532);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBoxType);
            this.Controls.Add(this.buttonAdminReturn);
            this.Controls.Add(this.buttonInstRegister);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxInstConfirmPassword);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxInstPhoneNumber);
            this.Controls.Add(this.textBoxInstAreaCode);
            this.Controls.Add(this.textBoxInstEmail);
            this.Controls.Add(this.textBoxInstSName);
            this.Controls.Add(this.textBoxInstFName);
            this.Controls.Add(this.textBoxInstPassword);
            this.Controls.Add(this.textBoxInstUserName);
            this.Name = "RegisterInstructor";
            this.Text = "New Staff Registration";
            this.Load += new System.EventHandler(this.RegisterInstructor_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxInstConfirmPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxInstPhoneNumber;
        private System.Windows.Forms.TextBox textBoxInstAreaCode;
        private System.Windows.Forms.TextBox textBoxInstEmail;
        private System.Windows.Forms.TextBox textBoxInstSName;
        private System.Windows.Forms.TextBox textBoxInstFName;
        private System.Windows.Forms.TextBox textBoxInstPassword;
        private System.Windows.Forms.TextBox textBoxInstUserName;
        private System.Windows.Forms.Button buttonInstRegister;
        private System.Windows.Forms.Button buttonAdminReturn;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.Label label8;
    }
}