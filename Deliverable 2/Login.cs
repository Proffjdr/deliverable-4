﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Deliverable_2
{
    public partial class Login : Form
    {
        public static string User="";
        public Login()
        {
            InitializeComponent();
            textBoxPassword.PasswordChar = '*';

        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            bool loginI = false, loginA = false, loginC = false;
            string uname = "", pword = "", fname = "", sname = "";

            if ("".Equals(textBoxUserName.Text.Trim()) || "".Equals(textBoxPassword.Text.Trim()))
            {
                MessageBox.Show("Enter your Username and Password");
                return;
            }

            try
            {
                
                uname = textBoxUserName.Text.Trim();
                pword = textBoxPassword.Text.Trim();
            }
            catch
            {
                //Error message, more useful when you are storing numbers etc. into the database.
                MessageBox.Show("Please reenter your Username and Password");
                return;
            }

            SQL.selectQuery("SELECT * FROM Instructor");
            if (SQL.read.HasRows)
            {

                while (SQL.read.Read())
                {

                    if (uname.Equals(SQL.read[0].ToString()) && pword.Equals(SQL.read[1].ToString()))
                    {
                        loginI = true;
                        fname = SQL.read[2].ToString();
                        sname = SQL.read[3].ToString();
                        break;
                    }

                }

            }
            if(loginI==false)
            {
                SQL.selectQuery("SELECT * FROM Admin");
                if (SQL.read.HasRows)
                {

                    while (SQL.read.Read())
                    {

                        if (uname.Equals(SQL.read[0].ToString()) && pword.Equals(SQL.read[1].ToString()))
                        {
                            loginA = true;
                            fname = SQL.read[2].ToString();
                            sname = SQL.read[3].ToString();
                            break;
                        }

                    }

                }
            }

            if (loginI == false && loginA == false)
            {
                SQL.selectQuery("SELECT * FROM Client");
                if (SQL.read.HasRows)
                {

                    while (SQL.read.Read())
                    {

                        if (uname.Equals(SQL.read[0].ToString()) && pword.Equals(SQL.read[1].ToString()))
                        {
                            loginC = true;
                            fname = SQL.read[2].ToString();
                            sname = SQL.read[3].ToString();
                            break;
                        }

                    }

                }

            }
            if (loginI == true)
            {
                MessageBox.Show($"Welcome Instructor {fname} {sname}");
                User = uname;
                this.Hide();
                InstructorHome register = new InstructorHome();
                register.ShowDialog();
                this.Close();
            }

            else
            {
                if (loginA == true)
                {
                    MessageBox.Show($"Welcome Admin {fname} {sname}");
                    User = uname;
                    this.Hide();
                    AdminHome register = new AdminHome();
                    register.ShowDialog();
                    this.Close();
                }
                else
                {
                    if (loginC == true)
                    {
                        MessageBox.Show($"Welcome Client {fname} {sname}");
                        User = uname;
                        this.Hide();
                        ClientHome register = new ClientHome();
                        register.ShowDialog();
                        this.Close();
                    }

                    else
                    {
                        MessageBox.Show("Invalid Username or Password");
                        loginI = false;
                        loginA = false;
                        loginC = false;
                    }
                }

                
            }
        }

        private void buttonRegister_Click(object sender, EventArgs e)
        {
            this.Hide();
            RegisterPage register = new RegisterPage();
            register.ShowDialog();
            this.Close();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        
    }
}
