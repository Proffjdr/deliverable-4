﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deliverable_2
{
    public partial class RegisterInstructor : Form
    {
        public RegisterInstructor()
        {
            InitializeComponent();
        }

        private void buttonAdminReturn_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminHome register = new AdminHome();
            register.ShowDialog();
            this.Close();
        }

        private bool checkContent()
        {
            bool holdsData = true;
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    if ("".Equals((c as TextBox).Text.Trim()))
                    {
                        holdsData = false;
                    }
                }

                if (c is ComboBox)
                {
                    if ("".Equals((c as ComboBox).Text.Trim()))
                    {
                        holdsData = false;
                    }
                }
            }
            return holdsData;
        }

        private void buttonInstRegister_Click(object sender, EventArgs e)
        {
            string uname = "", pword = "", confirmpword = "", fname = "", sname = "", email = "", areacode = "", phone = "", type = "";
            bool filledFields = checkContent();

            try
            {
                uname = textBoxInstUserName.Text.Trim();
                pword = textBoxInstPassword.Text.Trim();
                confirmpword = textBoxInstConfirmPassword.Text.Trim();
                fname = textBoxInstFName.Text.Trim();
                sname = textBoxInstSName.Text.Trim();
                email = textBoxInstEmail.Text.Trim();
                areacode = textBoxInstAreaCode.Text.Trim();
                phone = textBoxInstPhoneNumber.Text.Trim();
                type = comboBoxType.Text;
                
            }

            catch
            {
                MessageBox.Show("Please check your entries and try again");
            }

            if (filledFields == false)
            {
                MessageBox.Show("Please Complete all required fields");
                textBoxInstUserName.Focus();
                return;
            }

            if (pword != confirmpword)
            {
                MessageBox.Show("Please ensure both passwords match");
                textBoxInstPassword.Focus();
                return;
            }

            SQL.selectQuery($"Select Instructor.username, Admin.username from Instructor, Admin where username = '{uname}'");
            if (SQL.read.HasRows)
            {
                MessageBox.Show("This username already exists, please try another");
                textBoxInstUserName.Focus();
                return;
            }

            try
            {
                SQL.executeQuery($"INSERT into {type} values('{uname}','{pword}','{fname}','{sname}','{email}','{areacode}','{phone}')");
                SQL.selectQuery($"Select username from {type} where username = '{uname}'");
                if (SQL.read.HasRows)
                {
                    if (type == "Instructor")
                    {
                        SQL.executeQuery($"Insert into Available(Instructor_username, timeslot_day, timeslot_time) SELECT username, timeslot_day, timeslot_time FROM Instructor, Timeslot WHERE username like '{uname}' ");
                    }
                    MessageBox.Show($"You have successfully registered as {uname}");
                    this.Hide();
                    AdminHome register = new AdminHome();
                    register.ShowDialog();
                    this.Close();
                    return;
                }
                else
                {
                    MessageBox.Show($"Please ensure your password is at least 6 characters, and your email and phone information is valid.");
                    textBoxInstPassword.Focus();

                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Please check your fields and try again");
                return;
            }
        }

        private void RegisterInstructor_Load(object sender, EventArgs e)
        {
            comboBoxType.Items.Add("Instructor");
            comboBoxType.Items.Add("Admin");
        }
    }
}
