﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;

namespace Deliverable_2
{
    public partial class AdminHome : Form
    {
        public static string DateString;
        public AdminHome()
        {
            InitializeComponent();
            update();
        }

        public void update()
        {
            comboBoxAdminClient.Text = "";
            comboBoxAdminClient.Items.Clear();
            comboBoxAdminDate.Text = "";
            comboBoxAdminDate.Items.Clear();
            comboBoxAdminTime.Text = "";
            comboBoxAdminTime.Items.Clear();
            comboBoxAdminInst.Text = "";
            comboBoxAdminInst.Items.Clear();
            comboBoxAdminCar.Text = "";
            comboBoxAdminCar.Items.Clear();

            SQL.editComboBoxItems(comboBoxAdminClient, "Select DISTINCT Client_username from Appointment where Instructor_username in (Select Instructor_username from Available where Booked like '1') and Car IS NULL");
        }

        public void comboBoxAdminClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxAdminDate.Text = "";
            comboBoxAdminDate.Items.Clear();
            comboBoxAdminTime.Text = "";
            comboBoxAdminTime.Items.Clear();
            comboBoxAdminInst.Text = "";
            comboBoxAdminInst.Items.Clear();
            comboBoxAdminCar.Text = "";
            comboBoxAdminCar.Items.Clear();
            DateTime Date;
            
            SQL.selectQuery($"Select DISTINCT Timeslot_day from Appointment where Client_username like '{comboBoxAdminClient.Text}' and Car IS NULL");
             if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Date = DateTime.Parse($"{SQL.read[0].ToString()}");
                    DateString = Date.ToString("yyyy-MM-dd");
                    comboBoxAdminDate.Items.Add(DateString);
                }
            }
                
        }

        private void comboBoxAdminDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxAdminTime.Text = "";
            comboBoxAdminTime.Items.Clear();
            comboBoxAdminInst.Text = "";
            comboBoxAdminInst.Items.Clear();
            comboBoxAdminCar.Text = "";
            comboBoxAdminCar.Items.Clear();
            SQL.editComboBoxItems(comboBoxAdminTime, $"Select DISTINCT Timeslot_time from Appointment where Client_username like '{comboBoxAdminClient.Text}' AND Timeslot_day like '{comboBoxAdminDate.Text}' and Car IS NULL");
        }

        private void comboBoxAdminTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxAdminInst.Text = "";
            comboBoxAdminInst.Items.Clear();
            comboBoxAdminCar.Text = "";
            comboBoxAdminCar.Items.Clear();
            SQL.editComboBoxItems(comboBoxAdminInst, $"Select DISTINCT Instructor_username from Appointment where Client_username like '{comboBoxAdminClient.Text}' AND Timeslot_day like '{comboBoxAdminDate.Text}' AND Timeslot_time like '{comboBoxAdminTime.Text}' and Car IS NULL");
        }

        private void comboBoxAdminInst_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxAdminCar.Items.Clear();
            SQL.selectQuery($"SELECT DISTINCT licence from Assigned WHERE username like '{comboBoxAdminInst.Text}' AND  timeslot_day like '{comboBoxAdminDate.Text}'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    comboBoxAdminCar.Text = $"{SQL.read[0].ToString()}";
                }
            }
            else
            {
                comboBoxAdminCar.Text = "";
            }
            
            SQL.editComboBoxItems(comboBoxAdminCar, $"Select licence FROM CAR WHERE licence NOT IN (Select DISTINCT Car from Appointment WHERE Car.licence = Car AND (timeslot_day like '{comboBoxAdminDate.Text}' AND timeslot_time like '{comboBoxAdminTime.Text}')  OR Car IN (SELECT DISTINCT licence FROM Assigned WHERE Appointment.Car = Assigned.licence AND username NOT LIKE '{comboBoxAdminInst.Text}' AND timeslot_day like '{comboBoxAdminDate.Text}' AND timeslot_time like '{comboBoxAdminTime.Text}'))");
        }

        private void buttonAdminConfirm_Click(object sender, EventArgs e)
        {
            SQL.executeQuery($"UPDATE Appointment SET Car = '{comboBoxAdminCar.Text}', Confirmed = '1' WHERE Instructor_username like '{comboBoxAdminInst.Text}' AND timeslot_time like '{comboBoxAdminTime.Text}' AND Timeslot_day like '{DateString}' AND Client_username like '{comboBoxAdminClient.Text}'");
            MessageBox.Show("Appointment Confirmed!");
            emailClient($"{comboBoxAdminClient.Text}", true, $"{comboBoxAdminDate.Text}", $"{comboBoxAdminTime.Text}", $"{comboBoxAdminInst.Text}");
            update();
        }

        public void emailClient(string clientname, bool confirm, string date, string time, string instructor)
        {
            string email = "";
            string fname = "";
            string sname = "";
            SQL.selectQuery($"Select email from Client WHERE username like '{clientname}'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    email = $"{SQL.read[0].ToString()}";
                }
            }
            //Further Development of Email notification system goes here, for more info try http://stackoverflow.com/questions/9201239/send-e-mail-via-smtp-using-c-sharp 
            // Not implemented due to use of fake emails. Don't want to accidently spam anybody.

            SQL.selectQuery($"Select fname, sname from Instructor WHERE username like '{instructor}'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    fname = $"{SQL.read[0].ToString()}";
                    sname = $"{SQL.read[1].ToString()}";
                }
            }

            try
            {

                if (confirm == true)
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress("drivinginstructionacademy@gmail.com");
                        mail.To.Add(email);
                        mail.Subject = $"Booking on {date} Confirmed";
                        mail.Body = $"Your booking on {date} at {time} has been confirmed. It will be held by Instructor {fname} {sname}. We look forward to seeing you!";
                        mail.IsBodyHtml = true;
                        // Can set to false, if you are sending pure text.


                        using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                        {
                            smtp.Credentials = new NetworkCredential("drivinginstructionacademy@gmail.com", "drive123");
                            smtp.EnableSsl = true;
                            smtp.Send(mail);
                        }
                    }
                }
                else
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress("drivinginstructionacademy@gmail.com");
                        mail.To.Add(email);
                        mail.Subject = $"Booking on {date} Unavailable";
                        mail.Body = $"Your booking on {date} at {time} is Unavailable due to unforseen circumstances. Please get in touch so we can reschedule. We look forward to hearing from you soon";
                        mail.IsBodyHtml = true;
                        // Can set to false, if you are sending pure text.


                        using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                        {
                            smtp.Credentials = new NetworkCredential("drivinginstructionacademy@gmail.com", "drive123");
                            smtp.EnableSsl = true;
                            smtp.Send(mail);
                        }
                    }
                }
                update();
            }
            catch
            {
                MessageBox.Show("Unable to send confirmation email. Please Let this Client know thier appointment is confirmed.");
            }
        }

        private void buttonAdminReject_Click(object sender, EventArgs e)
        {
            SQL.executeQuery($"UPDATE Available SET Booked = '0' WHERE Instructor_username like '{comboBoxAdminInst.Text}' AND Timeslot_time like '{comboBoxAdminTime.Text}' AND Timeslot_day like '{DateString}'");
            SQL.executeQuery($"DELETE FROM Appointment WHERE Client_username like '{comboBoxAdminClient.Text}' AND Instructor_username like '{comboBoxAdminInst.Text}' AND Timeslot_day like '{DateString}' AND Timeslot_time like '{comboBoxAdminTime.Text}'");
            MessageBox.Show("Booking cancelled.");
            emailClient($"{comboBoxAdminClient.Text}", false, $"{comboBoxAdminDate.Text}", $"{comboBoxAdminTime.Text}", $"{comboBoxAdminInst}");
            update();
        }

        private void buttonAdminLogout_Click(object sender, EventArgs e)
        {
            Login.User = "";
            this.Hide();
            Login register = new Login();
            register.ShowDialog();
            this.Close();
        }

        private void buttonAdminRegisterInst_Click(object sender, EventArgs e)
        {
            this.Hide();
            RegisterInstructor register = new RegisterInstructor();
            register.ShowDialog();
            this.Close();
        }

        private void buttonCover_Click(object sender, EventArgs e)
        {
            this.Hide();
            Coverage register = new Coverage();
            register.ShowDialog();
            this.Close();
        }

        private void buttonRemoveInstructor_Click(object sender, EventArgs e)
        {
            this.Hide();
            Remove register = new Remove();
            register.ShowDialog();
            this.Close();
        }

        private void buttonAssignVehicles_Click(object sender, EventArgs e)
        {
            this.Hide();
            AssignVehicle register = new AssignVehicle();
            register.ShowDialog();
            this.Close();
        }

        private void buttonAdminInstAvail_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminInstreg register = new AdminInstreg();
            register.ShowDialog();
            this.Close();
        }

        private void buttonBilling_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminBilling register = new AdminBilling();
            register.ShowDialog();
            this.Close();
        }
    }

}
