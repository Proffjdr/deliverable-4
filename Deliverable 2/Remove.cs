﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deliverable_2
{
    public partial class Remove : Form
    {
        public Remove()
        {
            InitializeComponent();
        }

        private void Remove_Load(object sender, EventArgs e)
        {
            update();
        }

        private void update()
        {
            comboBoxRemove.Text = "";
            comboBoxRemove.Items.Clear();

            SQL.editComboBoxItems(comboBoxRemove, "Select username FROM Instructor UNION Select username FROM Admin");
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show($"Are you sure to delete {comboBoxRemove.Text} ??",
                                     "Confirm Delete!!",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                SQL.executeQuery($"DELETE FROM AVAILABLE WHERE Instructor_username like '{comboBoxRemove.Text}'");
                SQL.executeQuery($"DELETE from Instructor WHERE username like '{comboBoxRemove.Text}'");
                SQL.executeQuery($"DELETE from Admin WHERE username like '{comboBoxRemove.Text}'");
                MessageBox.Show($"User {comboBoxRemove.Text} removed");
                update();
            }
            else
            {
                update();
            }
           

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminHome register = new AdminHome();
            register.ShowDialog();
            this.Close();
        }
    }
}
