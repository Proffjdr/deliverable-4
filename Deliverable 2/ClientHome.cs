﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deliverable_2
{
    public partial class ClientHome : Form
    {
        public static string Date;
        public ClientHome()
        {
            InitializeComponent();
            monthCalendarClient.MaxSelectionCount = 1;
            
        }

        private void ClientHome_Load(object sender, EventArgs e)
        {
            refresh();
        }

        public void refresh()
        {
            comboBoxTime.Text = "";
            comboBoxTime.Items.Clear();
            comboBoxInstructor.Text = "";
            comboBoxInstructor.Items.Clear();
            
            Date = monthCalendarClient.SelectionRange.Start.ToString("yyyy-MM-dd");

            SQL.editComboBoxItems(comboBoxTime, $"Select DISTINCT timeslot_time FROM Available where timeslot_day like '{Date}' AND (Available like '1' AND Booked like '0')");
            labelInst.Text = "";
            
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            refresh();
        }

        private void comboBoxTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            SQL.editComboBoxItems(comboBoxInstructor, $"Select DISTINCT Instructor_username from Available where Available like '1' AND timeslot_day like '{Date}' AND timeslot_time like '{comboBoxTime.Text}' AND Booked like '0'");
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            Login.User = "";
            this.Hide();
            Login register = new Login();
            register.ShowDialog();
            this.Close();
        }

        private void buttonBook_Click(object sender, EventArgs e)
        {
            if(comboBoxTime.Text != "")
            {
                if(comboBoxInstructor.Text != "")
                {
                    SQL.executeQuery($"INSERT into Appointment (Instructor_username, Client_username, Timeslot_day, Timeslot_time) values ('{comboBoxInstructor.Text}','{Login.User}','{Date}','{comboBoxTime.Text}')");
                    SQL.executeQuery($"UPDATE Available SET Booked = '1' WHERE Instructor_username like '{comboBoxInstructor.Text}' and Timeslot_day like '{Date}' and Timeslot_time like '{comboBoxTime.Text}'");
                    MessageBox.Show($"Booking for {comboBoxTime.Text} on {Date} Sent! you will recieve email confirmation shortly");
                    
                    
                }
                else
                {
                    MessageBox.Show("Please Select a valid Instructor");
                }

            }
            else
            {
                MessageBox.Show("Please Select a valid Appointment Time.");
            }
            
        }

        private void comboBoxInstructor_SelectedIndexChanged(object sender, EventArgs e)
        {
            SQL.selectQuery($"Select * from Instructor where username like '{comboBoxInstructor.Text}'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    labelInst.Text = $"{SQL.read[2].ToString()} {SQL.read[3].ToString()}";
                }
            }
            
        }

        private void monthCalendarClient_DateChanged(object sender, DateRangeEventArgs e)
        {
            refresh();
        }
    }
}
