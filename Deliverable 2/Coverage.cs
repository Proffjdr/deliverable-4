﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deliverable_2
{
    public partial class Coverage : Form
    {
        public Coverage()
        {
            InitializeComponent();
            monthCalendarCover.MaxSelectionCount = 1;
            Cover();
        }

        public void Cover()
        {
            DateTime Date;
            SQL.selectQuery("Select DISTINCT Timeslot_day from Available WHERE (timeslot_time like '07:00:00' AND available like '1') AND (timeslot_time like '08:00:00' AND available like '1') AND (timeslot_time like '09:00:00' AND available like '1') AND (timeslot_time like '10:00:00' AND available like '1') AND (timeslot_time like '11:00:00' AND available like '1') AND (timeslot_time like '12:00:00' AND available like '1') AND (timeslot_time like '13:00:00' AND available like '1') AND (timeslot_time like '14:00:00' AND available like '1') AND (timeslot_time like '15:00:00' AND available like '1') AND (timeslot_time like '16:00:00' AND available like '1') AND (timeslot_time like '17:00:00' AND available like '1') AND (timeslot_time like '18:00:00' AND available like '1') AND (timeslot_time like '19:00:00' AND available like '1')");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Date = DateTime.Parse($"{SQL.read[0].ToString()}");
                    monthCalendarCover.AddBoldedDate(Date);
                }
            }

            

        }

        private void buttonCoverLogout_Click(object sender, EventArgs e)
        {
            Login.User = "";
            this.Hide();
            Login register = new Login();
            register.ShowDialog();
            this.Close();
        }

        private void buttonCoverReturn_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminHome register = new AdminHome();
            register.ShowDialog();
            this.Close();
        }

        private void monthCalendarCover_DateChanged(object sender, DateRangeEventArgs e)
        {
            Cover();
            DateTime Time;
            int i=0;
            List<Label> labels = new List<Label>();
            
            labels.Add(lblavil1); labels.Add(lblavil2); labels.Add(lblavil3); labels.Add(lblavil4); labels.Add(lblavil5); labels.Add(lblavil6); labels.Add(lblavil7); labels.Add(lblavil8); labels.Add(lblavil9); labels.Add(lblavil10); labels.Add(lblavil11); labels.Add(lblavil12); labels.Add(lblavil13);
            string Date = monthCalendarCover.SelectionRange.Start.ToString("yyyy-MM-dd");
            while (i < 13)
            {
                labels[i].Text = "";
                i++;
            }
            string Name = "";

            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '07:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[0].Text = Name;
            Name = "";

            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '08:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[1].Text = Name;
            Name = "";

            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '09:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[2].Text = Name;
            Name = "";
            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '10:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[3].Text = Name;
            Name = "";
            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '11:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[4].Text = Name;
            Name = "";
            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '12:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[5].Text = Name;
            Name = "";
            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '13:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[6].Text = Name;
            Name = "";

            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '14:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[7].Text = Name;
            Name = "";
            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '15:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[8].Text = Name;
            Name = "";
            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '16:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[9].Text = Name;
            Name = "";
            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '17:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[10].Text = Name;
            Name = "";
            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '18:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[11].Text = Name;
            Name = "";
            SQL.selectQuery($"Select DISTINCT Instructor_username from Available where Timeslot_day like '{Date}' AND Timeslot_time like '19:00:00' AND Available like '1'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    Name += $" {SQL.read[0].ToString()},";
                }
            }
            labels[12].Text = Name;
            Name = "";

        }

        private void buttonStats_Click(object sender, EventArgs e)
        {
            this.Hide();
            Stats register = new Stats();
            register.ShowDialog();
            this.Close();

        }
    }
}
