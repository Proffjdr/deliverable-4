﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Deliverable_2
{
    public partial class InstructorHome : Form
    {
        
        public InstructorHome()
        {
            InitializeComponent();
        }

        private void InstructorHome_Load(object sender, EventArgs e)
        {
            update();
        }

        private void update()
        {
            string StartDate;
            string EndDate;
            StartDate = monthCalendarInst.SelectionRange.Start.ToString("yyyy-MM-dd");
            EndDate = monthCalendarInst.SelectionRange.End.ToString("yyyy-MM-dd");

            
            SQL.FillGridView(gridAvailable, $"SELECT * from Available where Instructor_username like '{Login.User}' AND timeslot_day >= '{StartDate}' AND timeslot_day <= '{EndDate}'");
            gridAvailable.AllowUserToResizeRows = false;
            gridAvailable.AllowUserToResizeColumns = false;
            gridAvailable.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridAvailable.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridAvailable.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridAvailable.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            gridAvailable.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridAvailable.RowHeadersVisible = false;
            gridAvailable.Columns[0].ReadOnly = true;
            gridAvailable.Columns[1].ReadOnly = true;
            gridAvailable.Columns[2].ReadOnly = true;
            gridAvailable.Columns[4].ReadOnly = true;
            gridAvailable.AllowUserToAddRows = false;
            gridAvailable.Columns[0].HeaderText = "Instructor Name";
            gridAvailable.Columns[1].HeaderText = "Date";
            gridAvailable.Columns[2].HeaderText = "Time";
            gridAvailable.Columns[3].HeaderText = "Availability";
            gridAvailable.Columns[4].HeaderText = "Booked";



        }

        private void buttonSaveAvailability_Click(object sender, EventArgs e)
        {
            SQL.SaveGrid();
            update();
        }

        private void buttonDates_Click(object sender, EventArgs e)
        {
            update();
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
                Login.User = "";
                this.Hide();
                Login register = new Login();
                register.ShowDialog();
                this.Close();
        }



        private void monthCalendarInst_DateChanged(object sender, DateRangeEventArgs e)
        {
            update();
        }

        private void buttonConfirmDriven_Click(object sender, EventArgs e)
        {
            this.Hide();
            InstConfirmDriven register = new InstConfirmDriven();
            register.ShowDialog();
            this.Close();
        }
    }
    
}
