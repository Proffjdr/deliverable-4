﻿namespace Deliverable_2
{
    partial class Coverage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.monthCalendarCover = new System.Windows.Forms.MonthCalendar();
            this.buttonCoverLogout = new System.Windows.Forms.Button();
            this.buttonCoverReturn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblavil13 = new System.Windows.Forms.Label();
            this.lblavil12 = new System.Windows.Forms.Label();
            this.lblavil11 = new System.Windows.Forms.Label();
            this.lblavil10 = new System.Windows.Forms.Label();
            this.lblavil9 = new System.Windows.Forms.Label();
            this.lblavil8 = new System.Windows.Forms.Label();
            this.lblavil7 = new System.Windows.Forms.Label();
            this.lblavil6 = new System.Windows.Forms.Label();
            this.lblavil5 = new System.Windows.Forms.Label();
            this.lblavil4 = new System.Windows.Forms.Label();
            this.lblavil3 = new System.Windows.Forms.Label();
            this.lblavil2 = new System.Windows.Forms.Label();
            this.lblavil1 = new System.Windows.Forms.Label();
            this.buttonStats = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // monthCalendarCover
            // 
            this.monthCalendarCover.Location = new System.Drawing.Point(13, 13);
            this.monthCalendarCover.Name = "monthCalendarCover";
            this.monthCalendarCover.TabIndex = 0;
            this.monthCalendarCover.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarCover_DateChanged);
            // 
            // buttonCoverLogout
            // 
            this.buttonCoverLogout.Location = new System.Drawing.Point(681, 395);
            this.buttonCoverLogout.Name = "buttonCoverLogout";
            this.buttonCoverLogout.Size = new System.Drawing.Size(75, 23);
            this.buttonCoverLogout.TabIndex = 1;
            this.buttonCoverLogout.Text = "Logout";
            this.buttonCoverLogout.UseVisualStyleBackColor = true;
            this.buttonCoverLogout.Click += new System.EventHandler(this.buttonCoverLogout_Click);
            // 
            // buttonCoverReturn
            // 
            this.buttonCoverReturn.Location = new System.Drawing.Point(13, 394);
            this.buttonCoverReturn.Name = "buttonCoverReturn";
            this.buttonCoverReturn.Size = new System.Drawing.Size(75, 23);
            this.buttonCoverReturn.TabIndex = 2;
            this.buttonCoverReturn.Text = "Return";
            this.buttonCoverReturn.UseVisualStyleBackColor = true;
            this.buttonCoverReturn.Click += new System.EventHandler(this.buttonCoverReturn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(261, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "7am:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "8am:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(261, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "9am";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(261, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "10am";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(261, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "11am";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(261, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "12pm";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(261, 197);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "1pm";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(261, 230);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "2pm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(261, 261);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "3pm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(261, 291);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "4pm";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(261, 322);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "5pm";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(261, 357);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "6pm";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(261, 394);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "7pm";
            // 
            // lblavil13
            // 
            this.lblavil13.AutoSize = true;
            this.lblavil13.Location = new System.Drawing.Point(316, 394);
            this.lblavil13.Name = "lblavil13";
            this.lblavil13.Size = new System.Drawing.Size(0, 13);
            this.lblavil13.TabIndex = 30;
            // 
            // lblavil12
            // 
            this.lblavil12.AutoSize = true;
            this.lblavil12.Location = new System.Drawing.Point(316, 357);
            this.lblavil12.Name = "lblavil12";
            this.lblavil12.Size = new System.Drawing.Size(0, 13);
            this.lblavil12.TabIndex = 29;
            // 
            // lblavil11
            // 
            this.lblavil11.AutoSize = true;
            this.lblavil11.Location = new System.Drawing.Point(316, 322);
            this.lblavil11.Name = "lblavil11";
            this.lblavil11.Size = new System.Drawing.Size(0, 13);
            this.lblavil11.TabIndex = 28;
            // 
            // lblavil10
            // 
            this.lblavil10.AutoSize = true;
            this.lblavil10.Location = new System.Drawing.Point(316, 291);
            this.lblavil10.Name = "lblavil10";
            this.lblavil10.Size = new System.Drawing.Size(0, 13);
            this.lblavil10.TabIndex = 27;
            // 
            // lblavil9
            // 
            this.lblavil9.AutoSize = true;
            this.lblavil9.Location = new System.Drawing.Point(316, 261);
            this.lblavil9.Name = "lblavil9";
            this.lblavil9.Size = new System.Drawing.Size(0, 13);
            this.lblavil9.TabIndex = 26;
            // 
            // lblavil8
            // 
            this.lblavil8.AutoSize = true;
            this.lblavil8.Location = new System.Drawing.Point(316, 230);
            this.lblavil8.Name = "lblavil8";
            this.lblavil8.Size = new System.Drawing.Size(0, 13);
            this.lblavil8.TabIndex = 25;
            // 
            // lblavil7
            // 
            this.lblavil7.AutoSize = true;
            this.lblavil7.Location = new System.Drawing.Point(316, 197);
            this.lblavil7.Name = "lblavil7";
            this.lblavil7.Size = new System.Drawing.Size(0, 13);
            this.lblavil7.TabIndex = 24;
            // 
            // lblavil6
            // 
            this.lblavil6.AutoSize = true;
            this.lblavil6.Location = new System.Drawing.Point(316, 162);
            this.lblavil6.Name = "lblavil6";
            this.lblavil6.Size = new System.Drawing.Size(0, 13);
            this.lblavil6.TabIndex = 23;
            // 
            // lblavil5
            // 
            this.lblavil5.AutoSize = true;
            this.lblavil5.Location = new System.Drawing.Point(316, 129);
            this.lblavil5.Name = "lblavil5";
            this.lblavil5.Size = new System.Drawing.Size(0, 13);
            this.lblavil5.TabIndex = 22;
            // 
            // lblavil4
            // 
            this.lblavil4.AutoSize = true;
            this.lblavil4.Location = new System.Drawing.Point(316, 100);
            this.lblavil4.Name = "lblavil4";
            this.lblavil4.Size = new System.Drawing.Size(0, 13);
            this.lblavil4.TabIndex = 21;
            // 
            // lblavil3
            // 
            this.lblavil3.AutoSize = true;
            this.lblavil3.Location = new System.Drawing.Point(316, 68);
            this.lblavil3.Name = "lblavil3";
            this.lblavil3.Size = new System.Drawing.Size(0, 13);
            this.lblavil3.TabIndex = 20;
            // 
            // lblavil2
            // 
            this.lblavil2.AutoSize = true;
            this.lblavil2.Location = new System.Drawing.Point(316, 42);
            this.lblavil2.Name = "lblavil2";
            this.lblavil2.Size = new System.Drawing.Size(0, 13);
            this.lblavil2.TabIndex = 19;
            // 
            // lblavil1
            // 
            this.lblavil1.AutoSize = true;
            this.lblavil1.Location = new System.Drawing.Point(316, 13);
            this.lblavil1.Name = "lblavil1";
            this.lblavil1.Size = new System.Drawing.Size(0, 13);
            this.lblavil1.TabIndex = 18;
            // 
            // buttonStats
            // 
            this.buttonStats.Location = new System.Drawing.Point(13, 188);
            this.buttonStats.Name = "buttonStats";
            this.buttonStats.Size = new System.Drawing.Size(96, 55);
            this.buttonStats.TabIndex = 31;
            this.buttonStats.Text = "Statistics";
            this.buttonStats.UseVisualStyleBackColor = true;
            this.buttonStats.Click += new System.EventHandler(this.buttonStats_Click);
            // 
            // Coverage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 430);
            this.Controls.Add(this.buttonStats);
            this.Controls.Add(this.lblavil13);
            this.Controls.Add(this.lblavil12);
            this.Controls.Add(this.lblavil11);
            this.Controls.Add(this.lblavil10);
            this.Controls.Add(this.lblavil9);
            this.Controls.Add(this.lblavil8);
            this.Controls.Add(this.lblavil7);
            this.Controls.Add(this.lblavil6);
            this.Controls.Add(this.lblavil5);
            this.Controls.Add(this.lblavil4);
            this.Controls.Add(this.lblavil3);
            this.Controls.Add(this.lblavil2);
            this.Controls.Add(this.lblavil1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonCoverReturn);
            this.Controls.Add(this.buttonCoverLogout);
            this.Controls.Add(this.monthCalendarCover);
            this.Name = "Coverage";
            this.Text = "Coverage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar monthCalendarCover;
        private System.Windows.Forms.Button buttonCoverLogout;
        private System.Windows.Forms.Button buttonCoverReturn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblavil13;
        private System.Windows.Forms.Label lblavil12;
        private System.Windows.Forms.Label lblavil11;
        private System.Windows.Forms.Label lblavil10;
        private System.Windows.Forms.Label lblavil9;
        private System.Windows.Forms.Label lblavil8;
        private System.Windows.Forms.Label lblavil7;
        private System.Windows.Forms.Label lblavil6;
        private System.Windows.Forms.Label lblavil5;
        private System.Windows.Forms.Label lblavil4;
        private System.Windows.Forms.Label lblavil3;
        private System.Windows.Forms.Label lblavil2;
        private System.Windows.Forms.Label lblavil1;
        private System.Windows.Forms.Button buttonStats;
    }
}