﻿namespace Deliverable_2
{
    partial class InstConfirmDriven
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBack = new System.Windows.Forms.Button();
            this.monthCalendarInst = new System.Windows.Forms.MonthCalendar();
            this.gridComplete = new System.Windows.Forms.DataGridView();
            this.buttonConfirm = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridComplete)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(617, 346);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(75, 23);
            this.buttonBack.TabIndex = 10;
            this.buttonBack.Text = "Back";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // monthCalendarInst
            // 
            this.monthCalendarInst.Location = new System.Drawing.Point(31, 12);
            this.monthCalendarInst.Name = "monthCalendarInst";
            this.monthCalendarInst.TabIndex = 9;
            this.monthCalendarInst.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarInst_DateChanged);
            // 
            // gridComplete
            // 
            this.gridComplete.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridComplete.Location = new System.Drawing.Point(270, 12);
            this.gridComplete.Name = "gridComplete";
            this.gridComplete.Size = new System.Drawing.Size(422, 328);
            this.gridComplete.TabIndex = 8;
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.Location = new System.Drawing.Point(144, 186);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(120, 45);
            this.buttonConfirm.TabIndex = 7;
            this.buttonConfirm.Text = "Save Changes";
            this.buttonConfirm.UseVisualStyleBackColor = true;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // InstConfirmDriven
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 399);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.monthCalendarInst);
            this.Controls.Add(this.gridComplete);
            this.Controls.Add(this.buttonConfirm);
            this.Name = "InstConfirmDriven";
            this.Text = "Instructor Complete Appointments";
            this.Load += new System.EventHandler(this.InstConfirmDriven_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridComplete)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.MonthCalendar monthCalendarInst;
        private System.Windows.Forms.DataGridView gridComplete;
        private System.Windows.Forms.Button buttonConfirm;
    }
}