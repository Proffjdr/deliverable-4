﻿namespace Deliverable_2
{
    partial class InstructorHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.availableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.johnathonProffitt_D2DataSet = new Deliverable_2.JohnathonProffitt_D2DataSet();
            this.availableTableAdapter = new Deliverable_2.JohnathonProffitt_D2DataSetTableAdapters.AvailableTableAdapter();
            this.fKAppointment286302ECBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.appointmentTableAdapter = new Deliverable_2.JohnathonProffitt_D2DataSetTableAdapters.AppointmentTableAdapter();
            this.buttonSaveAvailability = new System.Windows.Forms.Button();
            this.gridAvailable = new System.Windows.Forms.DataGridView();
            this.monthCalendarInst = new System.Windows.Forms.MonthCalendar();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.buttonConfirmDriven = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.availableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.johnathonProffitt_D2DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKAppointment286302ECBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAvailable)).BeginInit();
            this.SuspendLayout();
            // 
            // availableBindingSource
            // 
            this.availableBindingSource.DataMember = "Available";
            this.availableBindingSource.DataSource = this.johnathonProffitt_D2DataSet;
            // 
            // johnathonProffitt_D2DataSet
            // 
            this.johnathonProffitt_D2DataSet.DataSetName = "JohnathonProffitt_D2DataSet";
            this.johnathonProffitt_D2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // availableTableAdapter
            // 
            this.availableTableAdapter.ClearBeforeFill = true;
            // 
            // fKAppointment286302ECBindingSource
            // 
            this.fKAppointment286302ECBindingSource.DataMember = "FK__Appointment__286302EC";
            this.fKAppointment286302ECBindingSource.DataSource = this.availableBindingSource;
            // 
            // appointmentTableAdapter
            // 
            this.appointmentTableAdapter.ClearBeforeFill = true;
            // 
            // buttonSaveAvailability
            // 
            this.buttonSaveAvailability.Location = new System.Drawing.Point(131, 192);
            this.buttonSaveAvailability.Name = "buttonSaveAvailability";
            this.buttonSaveAvailability.Size = new System.Drawing.Size(120, 45);
            this.buttonSaveAvailability.TabIndex = 2;
            this.buttonSaveAvailability.Text = "Save Availability";
            this.buttonSaveAvailability.UseVisualStyleBackColor = true;
            this.buttonSaveAvailability.Click += new System.EventHandler(this.buttonSaveAvailability_Click);
            // 
            // gridAvailable
            // 
            this.gridAvailable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridAvailable.Location = new System.Drawing.Point(257, 18);
            this.gridAvailable.Name = "gridAvailable";
            this.gridAvailable.Size = new System.Drawing.Size(422, 328);
            this.gridAvailable.TabIndex = 3;
            // 
            // monthCalendarInst
            // 
            this.monthCalendarInst.Location = new System.Drawing.Point(18, 18);
            this.monthCalendarInst.Name = "monthCalendarInst";
            this.monthCalendarInst.TabIndex = 4;
            this.monthCalendarInst.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarInst_DateChanged);
            // 
            // buttonLogout
            // 
            this.buttonLogout.Location = new System.Drawing.Point(604, 352);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(75, 23);
            this.buttonLogout.TabIndex = 6;
            this.buttonLogout.Text = "Logout";
            this.buttonLogout.UseVisualStyleBackColor = true;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // buttonConfirmDriven
            // 
            this.buttonConfirmDriven.Location = new System.Drawing.Point(12, 330);
            this.buttonConfirmDriven.Name = "buttonConfirmDriven";
            this.buttonConfirmDriven.Size = new System.Drawing.Size(114, 45);
            this.buttonConfirmDriven.TabIndex = 7;
            this.buttonConfirmDriven.Text = "Complete Past Appointments";
            this.buttonConfirmDriven.UseVisualStyleBackColor = true;
            this.buttonConfirmDriven.Click += new System.EventHandler(this.buttonConfirmDriven_Click);
            // 
            // InstructorHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 393);
            this.Controls.Add(this.buttonConfirmDriven);
            this.Controls.Add(this.buttonLogout);
            this.Controls.Add(this.monthCalendarInst);
            this.Controls.Add(this.gridAvailable);
            this.Controls.Add(this.buttonSaveAvailability);
            this.Name = "InstructorHome";
            this.Text = "Instructor Home";
            this.Load += new System.EventHandler(this.InstructorHome_Load);
            ((System.ComponentModel.ISupportInitialize)(this.availableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.johnathonProffitt_D2DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fKAppointment286302ECBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAvailable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private JohnathonProffitt_D2DataSet johnathonProffitt_D2DataSet;
        private System.Windows.Forms.BindingSource availableBindingSource;
        private JohnathonProffitt_D2DataSetTableAdapters.AvailableTableAdapter availableTableAdapter;
        private System.Windows.Forms.BindingSource fKAppointment286302ECBindingSource;
        private JohnathonProffitt_D2DataSetTableAdapters.AppointmentTableAdapter appointmentTableAdapter;
        private System.Windows.Forms.Button buttonSaveAvailability;
        private System.Windows.Forms.DataGridView gridAvailable;
        private System.Windows.Forms.MonthCalendar monthCalendarInst;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.Button buttonConfirmDriven;
    }
}