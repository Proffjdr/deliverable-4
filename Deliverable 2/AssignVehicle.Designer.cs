﻿namespace Deliverable_2
{
    partial class AssignVehicle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxInst = new System.Windows.Forms.ComboBox();
            this.comboBoxDateFrom = new System.Windows.Forms.ComboBox();
            this.comboBoxDateTo = new System.Windows.Forms.ComboBox();
            this.comboBoxVehicle = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonAssign = new System.Windows.Forms.Button();
            this.buttonBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBoxInst
            // 
            this.comboBoxInst.FormattingEnabled = true;
            this.comboBoxInst.Location = new System.Drawing.Point(33, 60);
            this.comboBoxInst.Name = "comboBoxInst";
            this.comboBoxInst.Size = new System.Drawing.Size(121, 21);
            this.comboBoxInst.TabIndex = 0;
            // 
            // comboBoxDateFrom
            // 
            this.comboBoxDateFrom.FormattingEnabled = true;
            this.comboBoxDateFrom.Location = new System.Drawing.Point(160, 60);
            this.comboBoxDateFrom.Name = "comboBoxDateFrom";
            this.comboBoxDateFrom.Size = new System.Drawing.Size(121, 21);
            this.comboBoxDateFrom.TabIndex = 1;
            this.comboBoxDateFrom.SelectedIndexChanged += new System.EventHandler(this.comboBoxDateFrom_SelectedIndexChanged);
            // 
            // comboBoxDateTo
            // 
            this.comboBoxDateTo.FormattingEnabled = true;
            this.comboBoxDateTo.Location = new System.Drawing.Point(287, 60);
            this.comboBoxDateTo.Name = "comboBoxDateTo";
            this.comboBoxDateTo.Size = new System.Drawing.Size(121, 21);
            this.comboBoxDateTo.TabIndex = 2;
            this.comboBoxDateTo.SelectedIndexChanged += new System.EventHandler(this.comboBoxDateTo_SelectedIndexChanged);
            // 
            // comboBoxVehicle
            // 
            this.comboBoxVehicle.FormattingEnabled = true;
            this.comboBoxVehicle.Location = new System.Drawing.Point(415, 60);
            this.comboBoxVehicle.Name = "comboBoxVehicle";
            this.comboBoxVehicle.Size = new System.Drawing.Size(121, 21);
            this.comboBoxVehicle.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Instructor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(412, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Vehicle";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(157, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Date From";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(284, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Date To";
            // 
            // buttonAssign
            // 
            this.buttonAssign.Location = new System.Drawing.Point(542, 53);
            this.buttonAssign.Name = "buttonAssign";
            this.buttonAssign.Size = new System.Drawing.Size(121, 33);
            this.buttonAssign.TabIndex = 8;
            this.buttonAssign.Text = "Assign";
            this.buttonAssign.UseVisualStyleBackColor = true;
            this.buttonAssign.Click += new System.EventHandler(this.buttonAssign_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(542, 108);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(121, 47);
            this.buttonBack.TabIndex = 9;
            this.buttonBack.Text = "Back";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // AssignVehicle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 316);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.buttonAssign);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxVehicle);
            this.Controls.Add(this.comboBoxDateTo);
            this.Controls.Add(this.comboBoxDateFrom);
            this.Controls.Add(this.comboBoxInst);
            this.Name = "AssignVehicle";
            this.Text = "Assign Vehicles";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxInst;
        private System.Windows.Forms.ComboBox comboBoxDateFrom;
        private System.Windows.Forms.ComboBox comboBoxDateTo;
        private System.Windows.Forms.ComboBox comboBoxVehicle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonAssign;
        private System.Windows.Forms.Button buttonBack;
    }
}