﻿namespace Deliverable_2
{
    partial class AdminHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxAdminClient = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxAdminDate = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxAdminTime = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxAdminInst = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxAdminCar = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonAdminConfirm = new System.Windows.Forms.Button();
            this.buttonAdminReject = new System.Windows.Forms.Button();
            this.buttonAdminRegisterInst = new System.Windows.Forms.Button();
            this.buttonAdminLogout = new System.Windows.Forms.Button();
            this.buttonCover = new System.Windows.Forms.Button();
            this.buttonRemoveInstructor = new System.Windows.Forms.Button();
            this.buttonAssignVehicles = new System.Windows.Forms.Button();
            this.buttonBilling = new System.Windows.Forms.Button();
            this.buttonAdminInstAvail = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Appointments Awaiting Confirmation";
            // 
            // comboBoxAdminClient
            // 
            this.comboBoxAdminClient.FormattingEnabled = true;
            this.comboBoxAdminClient.Location = new System.Drawing.Point(12, 54);
            this.comboBoxAdminClient.Name = "comboBoxAdminClient";
            this.comboBoxAdminClient.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAdminClient.TabIndex = 2;
            this.comboBoxAdminClient.SelectedIndexChanged += new System.EventHandler(this.comboBoxAdminClient_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Client";
            // 
            // comboBoxAdminDate
            // 
            this.comboBoxAdminDate.FormattingEnabled = true;
            this.comboBoxAdminDate.Location = new System.Drawing.Point(143, 54);
            this.comboBoxAdminDate.Name = "comboBoxAdminDate";
            this.comboBoxAdminDate.Size = new System.Drawing.Size(129, 21);
            this.comboBoxAdminDate.TabIndex = 4;
            this.comboBoxAdminDate.SelectedIndexChanged += new System.EventHandler(this.comboBoxAdminDate_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(140, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Date";
            // 
            // comboBoxAdminTime
            // 
            this.comboBoxAdminTime.FormattingEnabled = true;
            this.comboBoxAdminTime.Location = new System.Drawing.Point(278, 54);
            this.comboBoxAdminTime.Name = "comboBoxAdminTime";
            this.comboBoxAdminTime.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAdminTime.TabIndex = 6;
            this.comboBoxAdminTime.SelectedIndexChanged += new System.EventHandler(this.comboBoxAdminTime_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(275, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Time";
            // 
            // comboBoxAdminInst
            // 
            this.comboBoxAdminInst.FormattingEnabled = true;
            this.comboBoxAdminInst.Location = new System.Drawing.Point(405, 54);
            this.comboBoxAdminInst.Name = "comboBoxAdminInst";
            this.comboBoxAdminInst.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAdminInst.TabIndex = 8;
            this.comboBoxAdminInst.SelectedIndexChanged += new System.EventHandler(this.comboBoxAdminInst_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(402, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Instructor";
            // 
            // comboBoxAdminCar
            // 
            this.comboBoxAdminCar.FormattingEnabled = true;
            this.comboBoxAdminCar.Location = new System.Drawing.Point(533, 54);
            this.comboBoxAdminCar.Name = "comboBoxAdminCar";
            this.comboBoxAdminCar.Size = new System.Drawing.Size(121, 21);
            this.comboBoxAdminCar.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(533, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Car";
            // 
            // buttonAdminConfirm
            // 
            this.buttonAdminConfirm.Location = new System.Drawing.Point(691, 25);
            this.buttonAdminConfirm.Name = "buttonAdminConfirm";
            this.buttonAdminConfirm.Size = new System.Drawing.Size(75, 23);
            this.buttonAdminConfirm.TabIndex = 12;
            this.buttonAdminConfirm.Text = "Confirm";
            this.buttonAdminConfirm.UseVisualStyleBackColor = true;
            this.buttonAdminConfirm.Click += new System.EventHandler(this.buttonAdminConfirm_Click);
            // 
            // buttonAdminReject
            // 
            this.buttonAdminReject.Location = new System.Drawing.Point(691, 70);
            this.buttonAdminReject.Name = "buttonAdminReject";
            this.buttonAdminReject.Size = new System.Drawing.Size(75, 23);
            this.buttonAdminReject.TabIndex = 13;
            this.buttonAdminReject.Text = "Reject";
            this.buttonAdminReject.UseVisualStyleBackColor = true;
            this.buttonAdminReject.Click += new System.EventHandler(this.buttonAdminReject_Click);
            // 
            // buttonAdminRegisterInst
            // 
            this.buttonAdminRegisterInst.Location = new System.Drawing.Point(12, 172);
            this.buttonAdminRegisterInst.Name = "buttonAdminRegisterInst";
            this.buttonAdminRegisterInst.Size = new System.Drawing.Size(121, 38);
            this.buttonAdminRegisterInst.TabIndex = 14;
            this.buttonAdminRegisterInst.Text = "Register Instructor";
            this.buttonAdminRegisterInst.UseVisualStyleBackColor = true;
            this.buttonAdminRegisterInst.Click += new System.EventHandler(this.buttonAdminRegisterInst_Click);
            // 
            // buttonAdminLogout
            // 
            this.buttonAdminLogout.Location = new System.Drawing.Point(818, 187);
            this.buttonAdminLogout.Name = "buttonAdminLogout";
            this.buttonAdminLogout.Size = new System.Drawing.Size(75, 23);
            this.buttonAdminLogout.TabIndex = 15;
            this.buttonAdminLogout.Text = "Logout";
            this.buttonAdminLogout.UseVisualStyleBackColor = true;
            this.buttonAdminLogout.Click += new System.EventHandler(this.buttonAdminLogout_Click);
            // 
            // buttonCover
            // 
            this.buttonCover.Location = new System.Drawing.Point(406, 172);
            this.buttonCover.Name = "buttonCover";
            this.buttonCover.Size = new System.Drawing.Size(121, 38);
            this.buttonCover.TabIndex = 16;
            this.buttonCover.Text = "Coverage";
            this.buttonCover.UseVisualStyleBackColor = true;
            this.buttonCover.Click += new System.EventHandler(this.buttonCover_Click);
            // 
            // buttonRemoveInstructor
            // 
            this.buttonRemoveInstructor.Location = new System.Drawing.Point(271, 172);
            this.buttonRemoveInstructor.Name = "buttonRemoveInstructor";
            this.buttonRemoveInstructor.Size = new System.Drawing.Size(129, 38);
            this.buttonRemoveInstructor.TabIndex = 19;
            this.buttonRemoveInstructor.Text = "Remove Instructor";
            this.buttonRemoveInstructor.UseVisualStyleBackColor = true;
            this.buttonRemoveInstructor.Click += new System.EventHandler(this.buttonRemoveInstructor_Click);
            // 
            // buttonAssignVehicles
            // 
            this.buttonAssignVehicles.Location = new System.Drawing.Point(660, 172);
            this.buttonAssignVehicles.Name = "buttonAssignVehicles";
            this.buttonAssignVehicles.Size = new System.Drawing.Size(118, 38);
            this.buttonAssignVehicles.TabIndex = 20;
            this.buttonAssignVehicles.Text = "Assign Vehicles";
            this.buttonAssignVehicles.UseVisualStyleBackColor = true;
            this.buttonAssignVehicles.Click += new System.EventHandler(this.buttonAssignVehicles_Click);
            // 
            // buttonBilling
            // 
            this.buttonBilling.Location = new System.Drawing.Point(533, 172);
            this.buttonBilling.Name = "buttonBilling";
            this.buttonBilling.Size = new System.Drawing.Size(121, 38);
            this.buttonBilling.TabIndex = 21;
            this.buttonBilling.Text = "Billing";
            this.buttonBilling.UseVisualStyleBackColor = true;
            this.buttonBilling.Click += new System.EventHandler(this.buttonBilling_Click);
            // 
            // buttonAdminInstAvail
            // 
            this.buttonAdminInstAvail.Location = new System.Drawing.Point(140, 172);
            this.buttonAdminInstAvail.Name = "buttonAdminInstAvail";
            this.buttonAdminInstAvail.Size = new System.Drawing.Size(125, 38);
            this.buttonAdminInstAvail.TabIndex = 22;
            this.buttonAdminInstAvail.Text = "Instructor Availability";
            this.buttonAdminInstAvail.UseVisualStyleBackColor = true;
            this.buttonAdminInstAvail.Click += new System.EventHandler(this.buttonAdminInstAvail_Click);
            // 
            // AdminHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 222);
            this.Controls.Add(this.buttonAdminInstAvail);
            this.Controls.Add(this.buttonBilling);
            this.Controls.Add(this.buttonAssignVehicles);
            this.Controls.Add(this.buttonRemoveInstructor);
            this.Controls.Add(this.buttonCover);
            this.Controls.Add(this.buttonAdminLogout);
            this.Controls.Add(this.buttonAdminRegisterInst);
            this.Controls.Add(this.buttonAdminReject);
            this.Controls.Add(this.buttonAdminConfirm);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBoxAdminCar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBoxAdminInst);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxAdminTime);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBoxAdminDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxAdminClient);
            this.Controls.Add(this.label1);
            this.Name = "AdminHome";
            this.Text = "AdminHome";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxAdminClient;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxAdminDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxAdminTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxAdminInst;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxAdminCar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonAdminConfirm;
        private System.Windows.Forms.Button buttonAdminReject;
        private System.Windows.Forms.Button buttonAdminRegisterInst;
        private System.Windows.Forms.Button buttonAdminLogout;
        private System.Windows.Forms.Button buttonCover;
        private System.Windows.Forms.Button buttonRemoveInstructor;
        private System.Windows.Forms.Button buttonAssignVehicles;
        private System.Windows.Forms.Button buttonBilling;
        private System.Windows.Forms.Button buttonAdminInstAvail;
    }
}