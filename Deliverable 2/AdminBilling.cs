﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;

namespace Deliverable_2
{
    public partial class AdminBilling : Form
    {
        public AdminBilling()
        {
            InitializeComponent();
        }

        private void AdminBilling_Load(object sender, EventArgs e)
        {
            update();
        }

        private void update()
        {
            comboBoxClient.Text = "";
            comboBoxStatus.Text = "";
            comboBoxClient.Items.Clear();
            comboBoxStatus.Items.Clear();
            listBoxBilling.Items.Clear();
            listBoxPayment.Items.Clear();

            SQL.FillList(listBoxBilling, "SELECT DISTINCT Client_username from Appointment WHERE confirmed like '1' AND Billed like '0'");
            SQL.FillList(listBoxPayment, "SELECT DISTINCT Client_username from Appointment WHERE Billed like '1' AND Paid like '0'");
            SQL.editComboBoxItems(comboBoxClient, "SELECT Client_username from Appointment WHERE confirmed like '1' AND Paid like '0'");

        }

        private void comboBoxClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxStatus.Text = "";
            comboBoxStatus.Items.Clear();

            SQL.selectQuery($"SELECT COUNT(id) FROM Appointment WHERE Client_username like '{comboBoxClient.Text}' AND Complete like '1' AND Billed like '0'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    labelUnbilled.Text = SQL.read[0].ToString();
                }
            }

            SQL.selectQuery($"SELECT COUNT(id) FROM Appointment WHERE Client_username like '{comboBoxClient.Text}' AND Billed like '1' AND Paid like '0'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    labelUnpaid.Text = SQL.read[0].ToString();
                }
            }

            comboBoxStatus.Items.Add("Mark as Paid");
            comboBoxStatus.Items.Add("Bill Client");
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            if (comboBoxStatus.Text == "Mark as Paid")
            {
                SQL.executeQuery($"UPDATE Appointment SET Paid = '1' WHERE Client_username like '{comboBoxClient.Text}' AND Billed like '1' AND Paid like '0'");
                
                update();
            }

            else if (comboBoxStatus.Text == "Bill Client")
            {
                var test = 0;
                SQL.selectQuery($"SELECT COUNT(id) FROM Appointment WHERE Client_username like '{comboBoxClient.Text}' AND Complete like '1' AND Billed like '0'");
                if (SQL.read.HasRows)
                {
                    while (SQL.read.Read())
                    {
                        var read = $"{SQL.read[0].ToString()}";
                        test = int.Parse(read);
                    }
                }
                if(test > 0)
                {
                    BillClient();
                    SQL.executeQuery($"UPDATE Appointment SET Billed = '1' WHERE Client_username like '{comboBoxClient.Text}' AND Complete like '1' AND Paid like '0' AND Billed like '0'");
                    MessageBox.Show("Client has been marked as paid");
                    update();
                }
                else
                {
                    MessageBox.Show("This Client has no sessions waiting to get billed");
                }
            }
            else
            {
                MessageBox.Show("Please Select a Valid Action");
            }
           
        }

        private void BillClient()
        {
            
            var txt = "";
            var Appointments = new List<Tuple<string, string>>();
            Appointments.Clear();
            var fname = "";
            var sname = "";
            var email = "";
            var hours = 0;
            var rate = 50;
            SQL.selectQuery($"SELECT fname, sname, email from Client WHERE username like '{comboBoxClient.Text}'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    fname = $"{SQL.read[0].ToString()}";
                    sname = $"{SQL.read[1].ToString()}";
                    email = $"{SQL.read[2].ToString()}";
                }
            }
            SQL.selectQuery($"SELECT Timeslot_day, Timeslot_time FROM Appointment WHERE Client_username like '{comboBoxClient.Text}' AND Complete like '1' AND Billed like '0'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    var Date = DateTime.Parse($"{SQL.read[0].ToString()}");
                    var DateString = Date.ToString("yyyy-MM-dd");
                    Appointments.Add(Tuple.Create(DateString, SQL.read[1].ToString()));
                }
            }

            SQL.selectQuery($"SELECT COUNT(id) FROM Appointment WHERE Client_username like '{comboBoxClient.Text}' AND Complete like '1' AND Billed like '0'");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    var read = $"{SQL.read[0].ToString()}";
                    hours = int.Parse(read);
                }
            }

            txt = $"<html><body><p>Hello {fname} {sname},</p><p>This email is a bill for your lessons at the Driving Instruction Academy.</p><table cellspacing='10'><tr><th>Appointment Date</th><th>Appointment Time</th><th>Hours</th><th>Rate</th></tr>";

            foreach(var tuple in Appointments)
            {
                txt = txt + $"<tr><td>{tuple.Item1}</td><td>{tuple.Item2}</td><td>1</td><td>${rate}</td></tr>";
            }

            txt = txt + $"<tr><td></td><td><b>TOTAL:</b></td><td>{hours}</td><td>${hours * rate}</td></tr></table><p>We hope to hear from you regarding this bill soon</p><p>Thank you</p><p>The Driving Instruction Academy</p></body></html>";
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress("drivinginstructionacademy@gmail.com");
                    mail.To.Add(email);
                    mail.Subject = $"Bill for Driving Instruction Academy";
                    mail.Body = txt;
                    mail.IsBodyHtml = true;
                    // Can set to false, if you are sending pure text.


                    using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                    {
                        smtp.Credentials = new NetworkCredential("drivinginstructionacademy@gmail.com", "drive123");
                        smtp.EnableSsl = true;
                        smtp.Send(mail);
                    }
                }
                return;
            }
            catch
            {
                MessageBox.Show("Please send client billing manually.");
                return;
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminHome register = new AdminHome();
            register.ShowDialog();
            this.Close();

        }
    }
}
