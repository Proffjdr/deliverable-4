﻿namespace Deliverable_2
{
    partial class ClientHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxTime = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxInstructor = new System.Windows.Forms.ComboBox();
            this.monthCalendarClient = new System.Windows.Forms.MonthCalendar();
            this.buttonBook = new System.Windows.Forms.Button();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.labelInst = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxTime
            // 
            this.comboBoxTime.FormattingEnabled = true;
            this.comboBoxTime.Location = new System.Drawing.Point(96, 200);
            this.comboBoxTime.Name = "comboBoxTime";
            this.comboBoxTime.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTime.TabIndex = 1;
            this.comboBoxTime.SelectedIndexChanged += new System.EventHandler(this.comboBoxTime_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Time";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 231);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Instructor";
            // 
            // comboBoxInstructor
            // 
            this.comboBoxInstructor.FormattingEnabled = true;
            this.comboBoxInstructor.Location = new System.Drawing.Point(96, 228);
            this.comboBoxInstructor.Name = "comboBoxInstructor";
            this.comboBoxInstructor.Size = new System.Drawing.Size(121, 21);
            this.comboBoxInstructor.TabIndex = 4;
            this.comboBoxInstructor.SelectedIndexChanged += new System.EventHandler(this.comboBoxInstructor_SelectedIndexChanged);
            // 
            // monthCalendarClient
            // 
            this.monthCalendarClient.Location = new System.Drawing.Point(18, 18);
            this.monthCalendarClient.Name = "monthCalendarClient";
            this.monthCalendarClient.TabIndex = 5;
            this.monthCalendarClient.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendarClient_DateChanged);
            // 
            // buttonBook
            // 
            this.buttonBook.Location = new System.Drawing.Point(18, 279);
            this.buttonBook.Name = "buttonBook";
            this.buttonBook.Size = new System.Drawing.Size(279, 83);
            this.buttonBook.TabIndex = 6;
            this.buttonBook.Text = "BOOK";
            this.buttonBook.UseVisualStyleBackColor = true;
            this.buttonBook.Click += new System.EventHandler(this.buttonBook_Click);
            // 
            // buttonLogout
            // 
            this.buttonLogout.Location = new System.Drawing.Point(248, 18);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(57, 39);
            this.buttonLogout.TabIndex = 7;
            this.buttonLogout.Text = "Logout";
            this.buttonLogout.UseVisualStyleBackColor = true;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // labelInst
            // 
            this.labelInst.AutoSize = true;
            this.labelInst.Location = new System.Drawing.Point(224, 235);
            this.labelInst.Name = "labelInst";
            this.labelInst.Size = new System.Drawing.Size(0, 13);
            this.labelInst.TabIndex = 8;
            // 
            // ClientHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 382);
            this.Controls.Add(this.labelInst);
            this.Controls.Add(this.buttonLogout);
            this.Controls.Add(this.buttonBook);
            this.Controls.Add(this.monthCalendarClient);
            this.Controls.Add(this.comboBoxInstructor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxTime);
            this.Name = "ClientHome";
            this.Text = "Book an Appointment";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxInstructor;
        private System.Windows.Forms.MonthCalendar monthCalendarClient;
        private System.Windows.Forms.Button buttonBook;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.Label labelInst;
    }
}